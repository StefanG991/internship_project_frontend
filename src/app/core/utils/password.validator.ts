import { AbstractControl } from '@angular/forms';

export function PasswordValidator(
  control: AbstractControl
): { [key: string]: boolean } | null {
  const confirmPassword = control;
  const password =
    control.parent?.get('password') || control.parent?.get('newPassword');

  return password && confirmPassword && password.value !== confirmPassword.value
    ? { misMatch: true }
    : null;
}
