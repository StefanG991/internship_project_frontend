import { GeneralFilterInterface } from '../models/filters/general-filter.interface';

export const removeFalsyFromFilter = (
  filter: GeneralFilterInterface
): GeneralFilterInterface => {
  type ObjectKey = keyof typeof filter;

  Object.keys(filter).forEach((key: string) => {
    const propertyAcces = key as ObjectKey;
    if (!filter[propertyAcces]) {
      delete filter[propertyAcces];
    }
  });

  return filter;
};
