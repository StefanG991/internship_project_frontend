import { HttpParams } from '@angular/common/http';
import { GeneralFilterInterface } from '../models/filters/general-filter.interface';

export const createParams = (filter: GeneralFilterInterface): HttpParams => {
  let params = new HttpParams();
  type ObjectKey = keyof typeof filter;

  Object.keys(filter).forEach((key: string) => {
    const propertyAcces = key as ObjectKey;
    params = params.append(key, filter[propertyAcces]!);
  });

  return params;
};
