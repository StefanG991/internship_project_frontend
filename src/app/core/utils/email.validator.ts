import { AbstractControl } from '@angular/forms';

export function EmailValidator(
  control: AbstractControl
): { [key: string]: boolean } | null {
  return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(control.value)
    ? null
    : { invalidEmail: true };
}
