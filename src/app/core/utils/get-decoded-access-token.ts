import jwt_decode from 'jwt-decode';
import { UserInterface } from '../models/users/user.interface';

export function getDecodedAccessToken(
  token: string
): { user: UserInterface } | null {
  try {
    return jwt_decode(token);
  } catch (Error) {
    return null;
  }
}
