export const passwordRegex =
  '^(?=.*?[A-Za-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-+_(){}<>,;]).{6,}$';
