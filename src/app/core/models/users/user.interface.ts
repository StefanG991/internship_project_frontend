import { Gender } from '../../enums/gender.enum';
import { Role } from '../../enums/role.enum';

export interface UserInterface {
  _id?: string;
  name: string;
  email: string;
  password?: string;
  gender: Gender;
  dateOfBirth: Date;
  height: number;
  role?: Role;
}
