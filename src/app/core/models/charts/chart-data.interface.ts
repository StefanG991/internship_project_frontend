export interface SerieInterface {
  value: number;
  name: string | Date;
}

export interface ChartDataInterface {
  name: string;
  series: SerieInterface[];
}
