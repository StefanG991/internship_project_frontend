import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { WorkoutsRoutingModule } from './workouts-routing.module';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { SharedModule } from '../../shared/shared.module';
import { WorkoutsComponent } from './pages/workouts/workouts.component';
import { CreateEditWorkoutComponent } from './components/create-edit-workout/create-edit-workout.component';
import { WorkoutGraphComponent } from './components/workout-graph/workout-graph.component';

@NgModule({
  declarations: [WorkoutsComponent, CreateEditWorkoutComponent, WorkoutGraphComponent],
  imports: [
    CommonModule,
    WorkoutsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatSnackBarModule,
    MatDialogModule,
    MatIconModule,
    MatProgressSpinnerModule,
    MatDatepickerModule,
    MatNativeDateModule,
    SharedModule,
    MatRadioModule,
    MatPaginatorModule,
    MatButtonToggleModule,
    MatTabsModule,
    NgxChartsModule,
  ],
})
export class WorkoutsModule {}
