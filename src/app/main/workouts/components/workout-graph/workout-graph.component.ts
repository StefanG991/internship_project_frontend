import { Component, Input, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { ExerciseTypeService } from '../../../exercise-type/services/exercise-type.service';
import { ExerciseTypeInterface } from '../../../exercise-type/models/exercise-type.interface';
import { WorkoutFilterInterface } from '../../models/workout-filter.interface';
import { WorkoutInterface } from '../../models/workout.interface';
import { WorkoutsService } from '../../services/workouts.service';
import {
  ChartDataInterface,
  SerieInterface,
} from '../../../../core/models/charts/chart-data.interface';

@Component({
  selector: 'app-workout-graph',
  templateUrl: './workout-graph.component.html',
  styleUrls: [
    '../../../../styles/actions-container.scss',
    './workout-graph.component.scss',
  ],
})
export class WorkoutGraphComponent implements OnInit {
  @Input()
  id!: string;
  workouts: WorkoutInterface[] = [];
  filterForm!: FormGroup;
  filterValues: Partial<WorkoutFilterInterface> = {
    category: '',
    exerciseType: '',
    dateRange: {
      start: '',
      end: '',
    },
  };
  destroy$: Subject<void> = new Subject();
  exerciseTypes: ExerciseTypeInterface[] = [];
  dataToVisualize: ChartDataInterface[] = [];
  categorySeries: SerieInterface[] = [];
  yAxisLabel: string = '';
  exerciseFormArrays: string[] = [
    'repetitions',
    'weight',
    'time',
    'distance',
    'series',
  ];

  constructor(
    private exerciseTypesService: ExerciseTypeService,
    private workoutsService: WorkoutsService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.getExerciseTypes();
    this.createForm();
    this.formListener();
    this.getWorkouts();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  tickFormatting(series: string): string {
    return series.slice(0, 16);
  }

  getExerciseTypes(): void {
    this.exerciseTypesService
      .getExerciseTypes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.exerciseTypes = res;
      });
  }

  getWorkouts(): void {
    this.workoutsService
      .getWorkouts(this.id, 0, 0, this.filterValues, true)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.workouts = res.workouts;
        this.yAxisLabel = this.filterValues.exerciseType!;

        this.workouts.forEach((w) => {
          this.addToSerie(w);
        });

        this.dataToVisualize = [
          {
            name: this.filterValues.category!,
            series: this.categorySeries.length
              ? this.categorySeries
              : [{ name: '', value: 0 }],
          },
        ];
        this.emptySerie();
      });
  }

  emptySerie(): void {
    this.categorySeries = [];
  }

  addToSerie(workout: WorkoutInterface): void {
    if (this.filterValues.category && this.filterValues.exerciseType) {
      const name = new Date(workout.date).toString();
      type ObjectKey = keyof typeof workout.exercises[0];
      const accessProperty = this.filterValues.category as ObjectKey;

      if (this.filterValues.category === 'series') {
        this.categorySeries.push({
          name: name + Math.random() + Math.random(),
          value: workout.exercises[0][accessProperty] as number,
        });
      } else {
        const array1 = workout.exercises[0][accessProperty] as number[];
        array1.forEach((v) => {
          this.categorySeries.push({
            name: name + Math.random() + Math.random(),
            value: v,
          });
        });
      }
    }
  }

  applyFilters(reset?: boolean): void {
    if (reset) {
      this.filterForm.setValue({
        exerciseType: '',
        category: '',
        dateRange: {
          start: '',
          end: '',
        },
      });
    }

    this.getWorkouts();
  }

  createForm(): void {
    this.filterForm = this.fb.group({
      category: ['', Validators.required],
      exerciseType: ['', Validators.required],
      dateRange: this.fb.group({
        start: '',
        end: '',
      }),
    });
  }

  formListener(): void {
    this.filterForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.filterValues = value;
      });
  }

  getControl(control: string): AbstractControl | null {
    return this.filterForm.get(control);
  }
}
