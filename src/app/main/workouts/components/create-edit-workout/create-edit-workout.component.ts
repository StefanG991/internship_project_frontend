import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
  FormArray,
  FormControl,
} from '@angular/forms';
import {
  finalize,
  forkJoin,
  Observable,
  Subject,
  switchMap,
  takeUntil,
} from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { WorkoutsService } from '../../services/workouts.service';
import { ExercisesService } from '../../services/exercises.service';
import { ExerciseTypeService } from '../../../exercise-type/services/exercise-type.service';
import { WorkoutInterface } from '../../models/workout.interface';
import { ExerciseTypeInterface } from '../../../exercise-type/models/exercise-type.interface';
import { ExerciseInterface } from '../../models/exercise.interface';
import { ExerciseFormInterface } from '../../models/exercise-form.interface';

@Component({
  selector: 'app-create-edit-workout',
  templateUrl: './create-edit-workout.component.html',
  styleUrls: [
    './create-edit-workout.component.scss',
    '../../../../styles/spinner.scss',
  ],
})
export class CreateEditWorkoutComponent implements OnInit, OnDestroy {
  workoutForm!: FormGroup;
  exerciseTypes: ExerciseTypeInterface[] = [];
  destroy$: Subject<void> = new Subject();
  isLoading: boolean = false;
  exerciseFormArrays: string[] = ['repetitions', 'weight', 'time', 'distance'];

  constructor(
    private workoutsService: WorkoutsService,
    private exercisesService: ExercisesService,
    private exerciseTypesService: ExerciseTypeService,
    @Inject(MAT_DIALOG_DATA)
    public data: WorkoutInterface,
    private dialogRef: MatDialogRef<CreateEditWorkoutComponent>,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  get exerciseFormsArray(): FormArray {
    return this.workoutForm.get('exercises') as FormArray;
  }

  ngOnInit(): void {
    this.getExerciseTypes();
    this.createWorkoutForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  getExerciseTypes(): void {
    this.exerciseTypesService
      .getExerciseTypes()
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.exerciseTypes = res;
      });
  }

  createWorkoutForm(): void {
    this.workoutForm = this.fb.group({
      date: [this.data._id ? this.data.date : null, [Validators.required]],
      exercises: this.fb.array([]),
    });
    this.data.date
      ? this.data.exercises.forEach((e) => {
          this.addExerciseForm(e);
        })
      : this.addExerciseForm();
  }

  addExerciseForm(exercise?: ExerciseInterface): void {
    const exerciseForm = this.fb.group({
      exerciseType: [
        exercise ? exercise.exerciseType._id : null,
        [Validators.required],
      ],
      series: [exercise ? exercise.series : null, [Validators.required]],
      isWeightLifting: [
        exercise ? exercise.isWeightLifting : null,
        [Validators.required],
      ],
      repetitions: this.fb.array([]),
      weight: this.fb.array([]),
      time: this.fb.array([]),
      distance: this.fb.array([]),
    });

    exercise
      ? this.fillExerciseForms(exerciseForm, exercise)
      : this.triggerExerciseFormInputs(exerciseForm, false);

    this.exerciseFormsArray.push(exerciseForm);
  }

  fillExerciseForms(
    exerciseForm: FormGroup,
    exercise: ExerciseInterface
  ): void {
    type ObjectKey = keyof typeof exercise;

    this.exerciseFormArrays.forEach((property: string) => {
      const propertyAcces = property as ObjectKey;
      const propertyArray = exercise[propertyAcces] as number[];

      propertyArray?.forEach((r) => {
        this.getFormsFromExerciseForm(exerciseForm, property).push(
          this.fb.group({
            rep: [r, [Validators.required]],
          })
        );
      });
    });

    this.triggerExerciseFormInputs(exerciseForm, true);
  }

  getFormsFromExerciseForm(form: AbstractControl, name: string): FormArray {
    return form.get(name) as FormArray;
  }

  triggerExerciseFormInputs(exerciseForm: FormGroup, update: boolean): void {
    exerciseForm
      .get('isWeightLifting')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        exerciseForm.get('series')?.updateValueAndValidity();
      });

    exerciseForm
      .get('series')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        if (exerciseForm.get('isWeightLifting')?.dirty || update) {
          while (
            this.getFormsFromExerciseForm(exerciseForm, 'repetitions').length ||
            this.getFormsFromExerciseForm(exerciseForm, 'time').length
          ) {
            this.exerciseFormArrays.forEach((property: string) => {
              this.getFormsFromExerciseForm(exerciseForm, property).removeAt(0);
            });
          }

          [...Array(exerciseForm.get('series')?.value).keys()].forEach(() => {
            if (exerciseForm.get('isWeightLifting')?.value) {
              this.exerciseFormArrays
                .slice(0, 2)
                .forEach((property: string) => {
                  this.createNewFormsInsideExerciseForm(exerciseForm, property);
                });
            } else {
              this.exerciseFormArrays.slice(2).forEach((property: string) => {
                this.createNewFormsInsideExerciseForm(exerciseForm, property);
              });
            }
          });
        }
      });
  }

  createNewFormsInsideExerciseForm(
    exerciseForm: FormGroup,
    property: string
  ): void {
    this.getFormsFromExerciseForm(exerciseForm, property).push(
      this.fb.group({
        rep: [null, [Validators.required]],
      })
    );
  }

  onSubmit(): void {
    if (this.workoutForm.invalid) {
      return;
    }

    this.isLoading = true;
    const workoutId = this.data._id;
    const reqBody = {
      ...this.workoutForm.value,
      user: this.data.user,
    };

    reqBody.exercises = reqBody.exercises.map((e: ExerciseFormInterface) => {
      return {
        ...e,
        isWeightLifting: e.isWeightLifting,
        repetitions: e.repetitions?.map((r) => r.rep),
        weight: e.weight?.map((w) => w.rep),
        time: e.time?.map((t) => t.rep),
        distance: e.distance?.map((d) => d.rep),
      };
    });

    const array2: Observable<ExerciseInterface> = reqBody.exercises.map(
      (e: ExerciseInterface) => {
        return this.exercisesService.createExercise(e);
      }
    );

    if (workoutId) {
      const array1 = this.data.exercises.map((e) => {
        return this.exercisesService.deleteExercise(e._id);
      });

      forkJoin(array1)
        .pipe(
          switchMap((response) => {
            return forkJoin(array2).pipe(
              switchMap((response: ExerciseInterface[]) => {
                return this.workoutsService.updateWorkout(workoutId, {
                  ...reqBody,
                  exercises: response.map((e: ExerciseInterface) => e._id),
                });
              })
            );
          }),
          finalize(() => (this.isLoading = false)),
          takeUntil(this.destroy$)
        )
        .subscribe(this.getObserver('update'));
      return;
    }

    forkJoin(array2)
      .pipe(
        switchMap((response: ExerciseInterface[]) => {
          return this.workoutsService.createWorkout({
            ...reqBody,
            user: this.data.user,
            exercises: response.map((e: ExerciseInterface) => e._id),
          });
        }),
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(this.getObserver('create'));
  }

  getObserver(method: string): {
    next: (res: WorkoutInterface) => void;
    error: () => void;
  } {
    const isUpdate = method === 'update';

    return {
      next: (res: WorkoutInterface) => {
        this.dialogRef.close(res);
        this.snackBar.open(
          isUpdate ? 'Workout updated' : 'Workout created',
          'Close',
          {
            duration: 3000,
          }
        );
      },
      error: () => {
        this.snackBar.open(
          isUpdate ? 'Workout update failed' : 'Workout creation failed',
          'Close'
        );
      },
    };
  }

  deleteExerciseForm(i: number): void {
    this.exerciseFormsArray.removeAt(i);
  }

  getControl(control: string): AbstractControl | null {
    return this.workoutForm.get(control);
  }
}
