import { WorkoutInterface } from './workout.interface';

export interface WorkoutPaginationInterface {
  workouts: WorkoutInterface[];
  numOfWorkouts: number;
}
