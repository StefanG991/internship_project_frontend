import { ExerciseTypeInterface } from '../../exercise-type/models/exercise-type.interface';

export interface ExerciseFormInterface {
  _id?: string;
  exerciseType: ExerciseTypeInterface;
  isWeightLifting: string;
  series: number;
  repetitions?: NestedInputInterface[];
  weight?: NestedInputInterface[];
  time?: NestedInputInterface[];
  distance?: NestedInputInterface[];
}

interface NestedInputInterface {
  rep: number;
}
