import { ExerciseInterface } from './exercise.interface';

export interface WorkoutInterface {
  _id?: string;
  user: string;
  date: Date;
  exercises: ExerciseInterface[];
}
