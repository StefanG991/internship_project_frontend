import { ExerciseTypeEnum } from '../../exercise-type/models/exercise-type.enum';

export interface WorkoutFilterInterface {
  category?: string;
  exerciseType: string;
  muscleGroups?: ExerciseTypeEnum[];
  dateRange: {
    start: string;
    end: string;
  };
}
