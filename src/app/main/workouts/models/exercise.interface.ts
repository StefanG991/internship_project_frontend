import { ExerciseTypeInterface } from '../../exercise-type/models/exercise-type.interface';

export interface ExerciseInterface {
  _id: string;
  exerciseType: ExerciseTypeInterface;
  isWeightLifting: boolean;
  series: number;
  repetitions?: number[];
  weight?: number[];
  time?: number[];
  distance?: number[];
}
