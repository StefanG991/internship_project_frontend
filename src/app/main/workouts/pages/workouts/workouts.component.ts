import { AfterViewInit, Component, OnInit } from '@angular/core';
import { takeUntil, filter, finalize, switchMap, of, forkJoin } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { WorkoutsService } from '../../services/workouts.service';
import { ExercisesService } from '../../services/exercises.service';
import { CreateEditWorkoutComponent } from '../../components/create-edit-workout/create-edit-workout.component';
import { AbstractPaginator } from '../../../components/abstractions/abstract-paginator';
import { ExerciseInterface } from '../../models/exercise.interface';
import { WorkoutInterface } from '../../models/workout.interface';
import { WorkoutFilterInterface } from '../../models/workout-filter.interface';
import { ExerciseTypeEnum } from '../../../exercise-type/models/exercise-type.enum';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.component.html',
  styleUrls: [
    './workouts.component.scss',
    '../../../../styles/spinner.scss',
    '../../../../styles/actions-container.scss',
    '../../../../styles/table.scss',
  ],
})
export class WorkoutsComponent
  extends AbstractPaginator
  implements OnInit, AfterViewInit
{
  filterForm!: FormGroup;
  workouts: WorkoutInterface[] = [];
  isLoading: boolean = false;
  id!: string;
  filterValues: WorkoutFilterInterface = {
    exerciseType: '',
    muscleGroups: [],
    dateRange: {
      start: '',
      end: '',
    },
  };
  muscleGroupsArray: string[] = Object.values(ExerciseTypeEnum);
  hasResults: boolean = true;

  constructor(
    private dialog: MatDialog,
    private workoutsService: WorkoutsService,
    private exercisesService: ExercisesService,
    private snackBar: MatSnackBar,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.getData(this.skip, this.limit);
    this.formListener();
  }

  ngAfterViewInit(): void {
    this.subscribeToPaginatorChanges();
  }

  createForm(): void {
    this.filterForm = this.fb.group({
      exerciseType: [''],
      muscleGroups: [[]],
      dateRange: this.fb.group({
        start: '',
        end: '',
      }),
    });
  }

  formListener(): void {
    this.filterForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.filterValues = value;
      });
  }

  getData(skip?: number, limit?: number): void {
    this.isLoading = true;
    this.id = this.route.snapshot.paramMap.get('id')!;

    this.workoutsService
      .getWorkouts(
        this.id,
        skip ? skip : this.paginator.pageIndex + 1,
        limit ? limit : this.paginator.pageSize,
        this.filterValues
      )
      .pipe(
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res) => {
          this.workouts = res.workouts;
          this.hasResults = this.workouts.length ? true : false;
          this.count = res.numOfWorkouts;
        },
        () => {
          () => this.snackBar.open('Could not load workouts', 'Close');
        }
      );
  }

  applyFilters(reset?: boolean): void {
    this.paginator.pageIndex = 0;

    if (reset) {
      this.filterForm.setValue({
        exerciseType: '',
        muscleGroups: [],
        dateRange: {
          start: '',
          end: '',
        },
      });
    }

    this.workoutsService
      .getWorkouts(this.id, 0, this.paginator.pageSize, this.filterValues)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.workouts = res.workouts;
        this.hasResults = this.workouts.length ? true : false;
        this.count = res.numOfWorkouts;
      });
  }

  openCreateEdit(
    workout: Partial<WorkoutInterface>,
    duplicate?: boolean,
    user?: string
  ): void {
    if (duplicate) {
      workout = { ...workout, _id: undefined, user };
    }

    const dialogRef = this.dialog.open(CreateEditWorkoutComponent, {
      data: { ...workout },
    });

    dialogRef
      .afterClosed()
      .pipe(
        filter((res) => res),
        switchMap((res) => {
          return this.workoutsService.getWorkouts(
            this.id,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize,
            this.filterValues
          );
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((res) => {
        this.workouts = res.workouts;
        this.hasResults = this.workouts.length ? true : false;
        this.count = res.numOfWorkouts;
      });
  }

  onDelete(workout: WorkoutInterface) {
    this.snackBar.open('Deleting...');

    const array1 = workout.exercises.map((e) => {
      return this.exercisesService.deleteExercise(e._id);
    });

    forkJoin(array1)
      .pipe(
        switchMap(() => {
          return this.workoutsService.deleteWorkout(workout._id!).pipe(
            switchMap(() => {
              return this.workoutsService.getWorkouts(
                this.id,
                this.paginator.pageIndex + 1,
                this.paginator.pageSize,
                this.filterValues
              );
            }),
            takeUntil(this.destroy$)
          );
        })
      )
      .subscribe(
        (res) => {
          this.workouts = res.workouts;
          this.hasResults = this.workouts.length ? true : false;
          this.count = res.numOfWorkouts;
          this.snackBar.open('Workout deleted', 'Close', {
            duration: 3000,
          });
        },
        () => {
          this.snackBar.open('Failed to delete', 'Close', {
            duration: 3000,
          });
        }
      );
  }
}
