import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExerciseInterface } from '../models/exercise.interface';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExercisesService {
  constructor(private http: HttpClient) {}

  getBaseUrl(): string {
    return `${environment.apiUrl}/exercises`;
  }

  createExercise(exercise: ExerciseInterface): Observable<ExerciseInterface> {
    return this.http.post<ExerciseInterface>(`${this.getBaseUrl()}`, exercise);
  }

  updateExercise(
    id: string,
    exercise: ExerciseInterface
  ): Observable<ExerciseInterface> {
    return this.http.patch<ExerciseInterface>(
      `${this.getBaseUrl()}/${id}`,
      exercise
    );
  }

  deleteExercise(id: string): Observable<ExerciseInterface> {
    return this.http.delete<ExerciseInterface>(`${this.getBaseUrl()}/${id}`);
  }
}
