import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { WorkoutInterface } from '../models/workout.interface';
import { WorkoutPaginationInterface } from '../models/workout-pagination.interface';
import { WorkoutFilterInterface } from '../models/workout-filter.interface';
import { environment } from '../../../../environments/environment';
import { removeFalsyFromFilter } from 'src/app/core/utils/remove-falsy-from-filter';
import { createParams } from 'src/app/core/utils/create-params';

@Injectable({
  providedIn: 'root',
})
export class WorkoutsService {
  constructor(private http: HttpClient) {}

  getBaseUrl(): string {
    return `${environment.apiUrl}/workout`;
  }

  getWorkouts(
    id: string,
    skip: number,
    limit: number,
    filter: Partial<WorkoutFilterInterface>,
    forChart: boolean = false
  ): Observable<WorkoutPaginationInterface> {
    const newFilter = {
      exerciseType: filter.exerciseType!.toLowerCase(),
      muscleGroups: filter.muscleGroups ? filter.muscleGroups.join(',') : '',
      start:
        filter.dateRange!.start && filter.dateRange!.end
          ? filter.dateRange!.start
          : '',
      end:
        filter.dateRange!.start && filter.dateRange!.end
          ? filter.dateRange!.end
          : '',
      skip: skip ? skip.toString() : '',
      limit: limit ? limit.toString() : '',
    };

    let clearedFilter = removeFalsyFromFilter(newFilter);

    const params = createParams(clearedFilter);

    return this.http
      .get<WorkoutPaginationInterface>(`${this.getBaseUrl()}/${id}`, {
        params: params,
      })
      .pipe(
        map((res) => {
          return {
            ...res,
            workouts: res.workouts.map((w) => {
              return {
                ...w,
                date: new Date(w.date),
                exercises: forChart
                  ? w.exercises.filter((e) =>
                      e.exerciseType.name.includes(filter.exerciseType!)
                    )
                  : w.exercises,
              };
            }),
          };
        })
      );
  }

  createWorkout(workout: WorkoutInterface): Observable<WorkoutInterface> {
    return this.http.post<WorkoutInterface>(`${this.getBaseUrl()}`, workout);
  }

  updateWorkout(
    id: string,
    workout: WorkoutInterface
  ): Observable<WorkoutInterface> {
    return this.http.patch<WorkoutInterface>(
      `${this.getBaseUrl()}/${id}`,
      workout
    );
  }

  deleteWorkout(id: string): Observable<WorkoutInterface> {
    return this.http.delete<WorkoutInterface>(`${this.getBaseUrl()}/${id}`);
  }
}
