import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main.component';

const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'user',
        loadChildren: () =>
          import('./users/users.module').then((m) => m.UsersModule),
      },
      {
        path: 'measurements',
        loadChildren: () =>
          import('./measurements/measurements.module').then(
            (m) => m.MeasurementsModule
          ),
      },
      {
        path: 'exercise-type',
        loadChildren: () =>
          import('./exercise-type/exercise-type.module').then(
            (m) => m.ExerciseTypeModule
          ),
      },
      {
        path: 'workouts',
        loadChildren: () =>
          import('./workouts/workouts.module').then((m) => m.WorkoutsModule),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
