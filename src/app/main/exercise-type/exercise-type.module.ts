import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { SharedModule } from '../../shared/shared.module';
import { ExerciseTypeRoutingModule } from './exercise-type-routing.module';
import { ExerciseTypeComponent } from './pages/exercise-type/exercise-type.component';
import { CreateEditExerciseTypeComponent } from './components/create-edit-exercise-type/create-edit-exercise-type.component';
import { SearchExerciseTypePipe } from './pipes/search-exercise-type.pipe';

@NgModule({
  declarations: [
    ExerciseTypeComponent,
    SearchExerciseTypePipe,
    CreateEditExerciseTypeComponent,
  ],
  entryComponents: [CreateEditExerciseTypeComponent],
  imports: [
    CommonModule,
    ExerciseTypeRoutingModule,
    FormsModule,
    MatTableModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSnackBarModule,
    MatDialogModule,
    MatProgressSpinnerModule,
    SharedModule,
    MatSelectModule,
    MatIconModule,
  ],
})
export class ExerciseTypeModule {}
