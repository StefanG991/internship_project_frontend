import { Pipe, PipeTransform } from '@angular/core';
import { ExerciseTypeEnum } from '../models/exercise-type.enum';
import { ExerciseTypeInterface } from '../models/exercise-type.interface';
import { FilterInterface } from '../models/filter.interface';

@Pipe({
  name: 'searchExerciseType',
})
export class SearchExerciseTypePipe implements PipeTransform {
  transform(exerciseTypes: ExerciseTypeInterface[], filter: FilterInterface) {
    if (
      exerciseTypes.length === 0 ||
      (filter.name === '' && filter.muscleGroups.length === 0)
    ) {
      return exerciseTypes;
    } else {
      return exerciseTypes.filter((e) => {
        return (
          e.name.toLowerCase().includes(filter.name.toLowerCase()) &&
          (filter.muscleGroups.length === 0
            ? true
            : filter.muscleGroups.some((m: ExerciseTypeEnum) => {
                return e.muscleGroups.includes(m);
              }))
        );
      });
    }
  }
}
