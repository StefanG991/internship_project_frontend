import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ExerciseTypeInterface } from '../models/exercise-type.interface';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ExerciseTypeService {
  constructor(private http: HttpClient) {}

  getBaseUrl(): string {
    return `${environment.apiUrl}/exercise-type`;
  }

  createExerciseType(
    exercise: ExerciseTypeInterface
  ): Observable<ExerciseTypeInterface> {
    return this.http.post<ExerciseTypeInterface>(
      `${this.getBaseUrl()}`,
      exercise
    );
  }

  getExerciseTypes(): Observable<ExerciseTypeInterface[]> {
    return this.http.get<ExerciseTypeInterface[]>(this.getBaseUrl());
  }

  updateExerciseType(
    id: string,
    exercise: ExerciseTypeInterface
  ): Observable<ExerciseTypeInterface> {
    return this.http.patch<ExerciseTypeInterface>(
      `${this.getBaseUrl()}/${id}`,
      exercise
    );
  }

  deleteExerciseType(id: string): Observable<ExerciseTypeInterface> {
    return this.http.delete<ExerciseTypeInterface>(
      `${this.getBaseUrl()}/${id}`
    );
  }
}
