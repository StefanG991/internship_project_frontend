import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExerciseTypeComponent } from './pages/exercise-type/exercise-type.component';

const routes: Routes = [{ path: '', component: ExerciseTypeComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ExerciseTypeRoutingModule {}
