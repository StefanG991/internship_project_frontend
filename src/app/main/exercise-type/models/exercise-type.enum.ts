export enum ExerciseTypeEnum {
  Deltoids = 'deltoids',
  Pectorals = 'pectorals',
  Biceps = 'biceps',
  Triceps = 'triceps',
  LatissimusDorsi = 'latissimus dorsi',
  Trapezius = 'trapezius',
  Quadriceps = 'quadriceps',
  Glutes = 'glutes',
  Hamstrings = 'hamstrings',
  Calves = 'calves',
  Abdominals = 'abdominals',
}
