import { ExerciseTypeEnum } from './exercise-type.enum';

export interface FilterInterface {
  name: string;
  muscleGroups: ExerciseTypeEnum[];
}
