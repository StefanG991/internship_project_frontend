import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subject, takeUntil, filter, finalize } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ExerciseTypeService } from '../../services/exercise-type.service';
import { AuthenticationService } from '../../../../auth/services/authentication.service';
import { CreateEditExerciseTypeComponent } from '../../components/create-edit-exercise-type/create-edit-exercise-type.component';
import { ExerciseTypeInterface } from '../../models/exercise-type.interface';
import { FilterInterface } from '../../models/filter.interface';
import { Role } from '../../../../core/enums/role.enum';
import { ExerciseTypeEnum } from '../../models/exercise-type.enum';

@Component({
  selector: 'app-exercise-type',
  templateUrl: './exercise-type.component.html',
  styleUrls: [
    './exercise-type.component.scss',
    '../../../../styles/actions-container.scss',
    '../../../../styles/table.scss',
    '../../../../styles/spinner.scss',
  ],
})
export class ExerciseTypeComponent implements OnInit, OnDestroy {
  exerciseTypes: ExerciseTypeInterface[] = [];
  isLoading: boolean = false;
  destroy$: Subject<void> = new Subject();
  filterForm!: FormGroup;
  filterValues: FilterInterface = {
    name: '',
    muscleGroups: [],
  };
  muscleGroupsArray: string[] = Object.values(ExerciseTypeEnum);
  isAdmin: boolean = this.authService.getUserInfo().user.role === Role.Admin;

  constructor(
    private dialog: MatDialog,
    private authService: AuthenticationService,
    private exerciseTypeService: ExerciseTypeService,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.fetchExerciseTypes();
    this.createForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  createForm(): void {
    this.filterForm = this.fb.group({
      name: [''],
      muscleGroups: [[]],
    });
  }

  applyFilters(reset?: boolean): void {
    if (reset) {
      this.filterForm.setValue({
        name: '',
        muscleGroups: [],
      });
    }
    this.filterValues = this.filterForm.value;
  }

  fetchExerciseTypes(): void {
    this.isLoading = true;
    this.exerciseTypeService
      .getExerciseTypes()
      .pipe(
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res) => {
          this.exerciseTypes = res;
          this.applyFilters();
        },
        () => this.snackBar.open('Could not load the exercise types', 'Close')
      );
  }

  onDelete(id: string): void {
    this.snackBar.open('Deleting...');
    this.exerciseTypeService
      .deleteExerciseType(id)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        (res) => {
          this.exerciseTypes = this.exerciseTypes.filter((e) => e._id !== id);

          this.applyFilters();
          this.snackBar.open('Exercise type deleted', 'Close', {
            duration: 3000,
          });
        },
        () => {
          this.snackBar.open('Failed to delete', 'Close', {
            duration: 3000,
          });
        }
      );
  }

  openCreateEdit(exercise?: ExerciseTypeInterface | undefined): void {
    const dialogRef = this.dialog.open(CreateEditExerciseTypeComponent, {
      data: { ...exercise },
    });
    dialogRef
      .afterClosed()
      .pipe(
        filter((res) => res),
        takeUntil(this.destroy$)
      )
      .subscribe((res) => {
        const isNewExerciseType = !this.exerciseTypes.find(
          (e) => e._id === res._id
        );

        if (isNewExerciseType) {
          this.exerciseTypes.push(res);
          this.applyFilters();
          return;
        }

        this.exerciseTypes = this.exerciseTypes.map((e) => {
          if (e._id === res._id) {
            return res;
          } else {
            return e;
          }
        });

        this.applyFilters();
      });
  }
}
