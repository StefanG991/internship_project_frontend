import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { finalize, Subject, takeUntil } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ExerciseTypeService } from '../../services/exercise-type.service';
import { ExerciseTypeInterface } from '../../models/exercise-type.interface';
import { ExerciseTypeEnum } from '../../models/exercise-type.enum';

@Component({
  selector: 'app-create-edit-exercise-type',
  templateUrl: './create-edit-exercise-type.component.html',
  styleUrls: [
    '../../../../styles/forms.scss',
    '../../../../styles/create-edit-popup.scss',
    './create-edit-exercise-type.component.scss',
  ],
})
export class CreateEditExerciseTypeComponent implements OnInit, OnDestroy {
  exerciseTypeForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  isLoading: boolean = false;
  muscleGroupsArray: string[] = Object.values(ExerciseTypeEnum);

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: ExerciseTypeInterface,
    private dialogRef: MatDialogRef<CreateEditExerciseTypeComponent>,
    private fb: FormBuilder,
    private snackBar: MatSnackBar,
    private exerciseTypeService: ExerciseTypeService
  ) {}

  ngOnInit(): void {
    this.createForm();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  createForm(): void {
    this.exerciseTypeForm = this.fb.group({
      name: [null, [Validators.required]],
      muscleGroups: [null, [Validators.required]],
    });
    if (this.data?._id) {
      this.updateForm();
    }
  }

  updateForm(): void {
    this.exerciseTypeForm.setValue({
      name: this.data.name,
      muscleGroups: this.data.muscleGroups,
    });
  }

  resetUserChanges(): void {
    this.data?._id ? this.updateForm() : this.exerciseTypeForm.reset();
  }

  getObserver(method: string): {
    next: (res: ExerciseTypeInterface) => void;
    error: () => void;
  } {
    const isUpdate = method === 'update';
    return {
      next: (res: ExerciseTypeInterface) => {
        this.dialogRef.close(res);
        this.snackBar.open(
          isUpdate ? 'Exercise type updated' : 'Exercise type created',
          'Close',
          {
            duration: 3000,
          }
        );
      },
      error: () => {
        this.snackBar.open(
          isUpdate
            ? 'Exercise type update failed'
            : 'Exercise type creation failed',
          'Close'
        );
      },
    };
  }

  onSubmit(): void {
    const exerciseID = this.data?._id;

    if (this.exerciseTypeForm.invalid) {
      return;
    }

    this.isLoading = true;

    if (exerciseID) {
      this.exerciseTypeService
        .updateExerciseType(exerciseID, this.exerciseTypeForm.value)
        .pipe(
          finalize(() => (this.isLoading = false)),
          takeUntil(this.destroy$)
        )
        .subscribe(this.getObserver('update'));
      return;
    }

    this.exerciseTypeService
      .createExerciseType(this.exerciseTypeForm.value)
      .pipe(
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(this.getObserver('create'));
  }

  getControl(control: string): AbstractControl | null {
    return this.exerciseTypeForm.get(control);
  }
}
