import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MainComponent } from './main.component';
import { MainHeaderComponent } from './layout/components/main-header/main-header.component';

@NgModule({
  declarations: [MainComponent, MainHeaderComponent],
  imports: [CommonModule, MainRoutingModule, MatToolbarModule],
})
export class MainModule {}
