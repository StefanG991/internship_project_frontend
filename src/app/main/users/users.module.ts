import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { UserRoutingModule } from './users-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { UserComponent } from './pages/user/user.component';
import { UserPasswordUpdateComponent } from './components/user-password-update/user-password-update.component';
import { UserUpdateComponent } from './components/user-update/user-update.component';

@NgModule({
  declarations: [
    UserComponent,
    UserPasswordUpdateComponent,
    UserUpdateComponent,
  ],
  imports: [
    CommonModule,
    UserRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatNativeDateModule,
    MatDatepickerModule,
    SharedModule,
    MatTabsModule,
    MatIconModule,
    MatSnackBarModule,
    MatSelectModule,
  ],
})
export class UsersModule {}
