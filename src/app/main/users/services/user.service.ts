import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserInterface } from '../../../core/models/users/user.interface';
import { ChangePasswordInterface } from '../models/change-password.interface';
import { environment } from '../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(private http: HttpClient) {}

  getUser(id: string): Observable<UserInterface> {
    return this.http.get<UserInterface>(`${environment.apiUrl}/users/${id}`);
  }

  updateUser(id: string, user: UserInterface): Observable<UserInterface> {
    return this.http.patch<UserInterface>(
      `${environment.apiUrl}/users/${id}`,
      user
    );
  }

  changePass(
    id: string,
    changePass: ChangePasswordInterface
  ): Observable<ChangePasswordInterface> {
    return this.http.patch<ChangePasswordInterface>(
      `${environment.apiUrl}/auth/password/${id}`,
      changePass
    );
  }
}
