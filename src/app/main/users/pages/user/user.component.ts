import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { UserService } from '../../services/user.service';
import { UserInterface } from '../../../../core/models/users/user.interface';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['../../../../styles/forms.scss', './user.component.scss'],
})
export class UserComponent implements OnInit, OnDestroy {
  user!: UserInterface;
  id!: string;
  destroy$: Subject<void> = new Subject();

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.fetchUser();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  fetchUser(): void {
    this.id = this.route.snapshot.paramMap.get('id')!;
    this.userService
      .getUser(this.id)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.user = { ...res, dateOfBirth: new Date(res.dateOfBirth) };
      });
  }

  updateUserObject(user: UserInterface): void {
    this.user = user;
  }
}
