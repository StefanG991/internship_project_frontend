export interface ChangePasswordInterface {
  email: string;
  oldPassword: string;
  newPassword: string;
  confirmedNewPassword: string;
}
