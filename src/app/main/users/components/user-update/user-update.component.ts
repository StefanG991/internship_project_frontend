import {
  Component,
  OnInit,
  OnDestroy,
  Input,
  Output,
  EventEmitter,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, takeUntil } from 'rxjs';
import { UserService } from '../../services/user.service';
import { UserInterface } from '../../../../core/models/users/user.interface';
import { Gender } from '../../../../core/enums/gender.enum';

@Component({
  selector: 'app-user-update',
  templateUrl: './user-update.component.html',
  styleUrls: [
    '../../../../styles/forms.scss',
    '../../../../styles/user-form.scss',
    './user-update.component.scss',
  ],
})
export class UserUpdateComponent implements OnInit, OnDestroy {
  @Input()
  user!: UserInterface;
  @Input()
  id!: string;
  @Output() onUpdateUser: EventEmitter<UserInterface> = new EventEmitter();
  userForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  genderArray: string[] = Object.values(Gender);

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.userForm = this.fb.group({
      name: [null, [Validators.required]],
      gender: [null, [Validators.required]],
      dateOfBirth: [null, [Validators.required]],
      height: [null, [Validators.required]],
    });
    this.updateForm(this.user);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  updateForm(user: UserInterface): void {
    this.userForm.setValue({
      name: user.name,
      gender: user.gender,
      dateOfBirth: user.dateOfBirth,
      height: user.height,
    });
  }

  resetUserChanges(): void {
    this.updateForm(this.user);
  }

  submitUserUpdate(): void {
    if (this.userForm.invalid) {
      return;
    }
    this.userService
      .updateUser(this.id, this.userForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.user = { ...res, dateOfBirth: new Date(res.dateOfBirth) };
        this.updateForm(res);
        this.snackBar.open('User Info Updated', 'Close', {
          duration: 3000,
        });
        this.onUpdateUser.emit(this.user);
      });
  }

  getControl(control: string): AbstractControl | null {
    return this.userForm.get(control);
  }
}
