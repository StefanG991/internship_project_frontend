import { Component, OnInit, OnDestroy, Input } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  Validators,
} from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subject, takeUntil, finalize } from 'rxjs';
import { UserService } from '../../services/user.service';
import { PasswordValidator } from '../../../../core/utils/password.validator';
import { passwordRegex } from '../../../../core/constants/regex';

@Component({
  selector: 'app-user-password-update',
  templateUrl: './user-password-update.component.html',
  styleUrls: [
    '../../../../styles/forms.scss',
    '../../../../styles/user-form.scss',
    './user-password-update.component.scss',
  ],
})
export class UserPasswordUpdateComponent implements OnInit, OnDestroy {
  @Input()
  id!: string;
  passwordForm!: FormGroup;
  destroy$: Subject<void> = new Subject();

  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.passwordForm = this.fb.group({
      email: [null, [Validators.required, Validators.email]],
      oldPassword: [null, [Validators.required]],
      newPassword: [
        null,
        [Validators.required, Validators.pattern(passwordRegex)],
      ],
      confirmedNewPassword: [null, [Validators.required, PasswordValidator]],
    });
    this.triggerConfirmPasswordValidation();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  triggerConfirmPasswordValidation(): void {
    this.passwordForm
      .get('newPassword')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.passwordForm.get('confirmedNewPassword')?.updateValueAndValidity();
      });
  }

  resetPasswordChanges(): void {
    this.passwordForm.reset();
  }

  submitPasswordChange(formDirective: FormGroupDirective): void {
    if (this.passwordForm.invalid) {
      return;
    }
    this.userService
      .changePass(this.id, this.passwordForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe(
        () => {
          formDirective.resetForm();
          this.resetPasswordChanges();
          this.snackBar.open('Password changed', 'Close', {
            duration: 3000,
          });
        },
        (err) => {
          this.snackBar.open(`${err.error.message}`, 'Close', {
            duration: 3000,
          });
        }
      );
  }

  getControl(control: string): AbstractControl | null {
    return this.passwordForm.get(control);
  }
}
