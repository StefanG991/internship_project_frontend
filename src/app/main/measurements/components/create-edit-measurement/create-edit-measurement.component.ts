import { Component, OnDestroy, OnInit, Inject } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Subject, switchMap, takeUntil, finalize } from 'rxjs';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MeasurementsService } from '../../services/measurements.service';
import { MeasurementInterface } from '../../models/measurement.interface';

@Component({
  selector: 'app-create-edit-measurement',
  templateUrl: './create-edit-measurement.component.html',
  styleUrls: [
    '../../../../styles/forms.scss',
    '../../../../styles/create-edit-popup.scss',
    './create-edit-measurement.component.scss',
  ],
})
export class CreateEditMeasurementComponent implements OnInit, OnDestroy {
  photoUrl: string | undefined = this.data?.photoUrl;
  photoId: string | undefined = this.data?.photoId;
  measurementForm!: FormGroup;
  photoForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  isLoading: boolean = false;
  public_id!: string;

  constructor(
    @Inject(MAT_DIALOG_DATA)
    public data: Partial<MeasurementInterface>,
    private dialogRef: MatDialogRef<CreateEditMeasurementComponent>,
    private fb: FormBuilder,
    private measurementService: MeasurementsService,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.createForms();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  createForms(): void {
    this.photoForm = this.fb.group({
      file: [null, [Validators.required]],
      fileSource: [null, [Validators.required]],
    });
    this.measurementForm = this.fb.group({
      weight: [null, [Validators.required]],
      chest: [null, [Validators.required]],
      waist: [null, [Validators.required]],
      hips: [null, [Validators.required]],
      biceps: [null, [Validators.required]],
      date: [null, [Validators.required]],
    });
    if (this.data?._id) {
      this.updateForm();
    }
  }

  updateForm(): void {
    this.measurementForm.setValue({
      weight: this.data?.weight,
      chest: this.data?.chest,
      waist: this.data?.waist,
      hips: this.data?.hips,
      biceps: this.data?.biceps,
      date: this.data?.date,
    });
  }

  resetUserChanges(): void {
    this.photoForm.reset();
    this.data?._id ? this.updateForm() : this.measurementForm.reset();
  }

  onSubmit(): void {
    const fileSource = this.photoForm.get('fileSource')?.value;
    const fd = new FormData();
    fd.append('file', fileSource);
    const measurementID = this.data?._id;

    if (measurementID && this.measurementForm.invalid) {
      return;
    }

    if (measurementID && fileSource) {
      this.isLoading = true;
      this.measurementService
        .deletePhoto(this.photoId!)
        .pipe(
          switchMap(() => {
            return this.measurementService.uploadPhoto(fd).pipe(
              switchMap((res) => {
                this.photoUrl = res.url;
                this.photoId = res.public_id;
                return this.measurementService.updateMeasurement(
                  measurementID,
                  {
                    ...this.measurementForm.value,
                    photoUrl: this.photoUrl,
                    photoId: this.photoId,
                  }
                );
              })
            );
          }),
          finalize(() => (this.isLoading = false)),
          takeUntil(this.destroy$)
        )
        .subscribe(
          (m) => {
            this.photoForm.reset();
            this.dialogRef.close(m);
            this.snackBar.open('Measurement updated', 'Close', {
              duration: 3000,
            });
          },
          (e) => {
            this.snackBar.open('Measurement update failed', 'Close');
          }
        );
      return;
    }

    if (measurementID && !fileSource) {
      this.isLoading = true;
      this.measurementService
        .updateMeasurement(measurementID, {
          ...this.measurementForm.value,
        })
        .pipe(
          finalize(() => (this.isLoading = false)),
          takeUntil(this.destroy$)
        )
        .subscribe(
          (m) => {
            this.dialogRef.close(m);
            this.snackBar.open('Measurement updated', 'Close', {
              duration: 3000,
            });
          },
          (e) => {
            this.snackBar.open('Measurement update failed', 'Close');
          }
        );
      return;
    }

    if (
      (!measurementID && this.measurementForm.invalid) ||
      this.photoForm.invalid
    ) {
      return;
    }

    this.isLoading = true;
    this.measurementService
      .uploadPhoto(fd)
      .pipe(
        switchMap((res) => {
          const { url } = res;
          this.public_id = res.public_id;
          return this.measurementService.createMeasurement({
            user: this.data.user,
            ...this.measurementForm.value,
            photoUrl: url,
            photoId: this.public_id,
          });
        }),
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (m) => {
          this.photoForm.reset();
          this.dialogRef.close(m);
          this.snackBar.open('Measurement created', 'Close', {
            duration: 3000,
          });
        },
        (e) => {
          this.measurementService.deletePhoto(this.public_id).subscribe(() => {
            this.snackBar.open(`${e.error.message}`, 'Close');
          });
        }
      );
  }

  getControl(control: string): AbstractControl | null {
    return this.measurementForm.get(control);
  }

  onPhotoSelected(event: Event): void {
    const element = event.target as HTMLInputElement;
    const selectedFile = <File>element.files![0];
    this.photoForm.patchValue({
      fileSource: selectedFile,
    });
  }
}
