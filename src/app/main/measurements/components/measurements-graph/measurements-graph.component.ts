import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Subject, takeUntil } from 'rxjs';
import { MeasurementsService } from '../../services/measurements.service';
import {
  ChartDataInterface,
  SerieInterface,
} from '../../../../core/models/charts/chart-data.interface';
import { GeneralFilterInterface } from '../../../../core/models/filters/general-filter.interface';
import { MeasurementInterface } from '../../models/measurement.interface';

@Component({
  selector: 'app-measurements-graph',
  templateUrl: './measurements-graph.component.html',
  styleUrls: [
    '../../../../styles/actions-container.scss',
    './measurements-graph.component.scss',
  ],
})
export class MeasurementsGraphComponent implements OnInit {
  @Input()
  id!: string;
  measurements: MeasurementInterface[] = [];
  filterForm!: FormGroup;
  filterValues: GeneralFilterInterface = {
    start: '',
    end: '',
  };
  destroy$: Subject<void> = new Subject();
  dataToVisualize: ChartDataInterface[] = [];
  bicepsSeries: SerieInterface[] = [];
  waistSeries: SerieInterface[] = [];
  hipsSeries: SerieInterface[] = [];
  weightSeries: SerieInterface[] = [];
  chestSeries: SerieInterface[] = [];

  constructor(
    private fb: FormBuilder,
    private measurementService: MeasurementsService
  ) {}

  ngOnInit(): void {
    this.createForm();
    this.formListener();
    this.getMeasurements();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  getMeasurements(): void {
    this.measurementService
      .getMeasurements(this.id, 0, 0, this.filterValues)
      .pipe(takeUntil(this.destroy$))
      .subscribe((res) => {
        this.measurements = res.measurements;

        this.measurements.forEach((m) => {
          this.addToSeries(m);
        });

        this.dataToVisualize = [
          {
            name: 'waist',
            series: this.waistSeries.length
              ? this.waistSeries
              : [{ name: '', value: 0 }],
          },
          {
            name: 'weight',
            series: this.weightSeries,
          },
          {
            name: 'hips',
            series: this.hipsSeries,
          },
          {
            name: 'biceps',
            series: this.bicepsSeries,
          },
          {
            name: 'chest',
            series: this.chestSeries,
          },
        ];

        this.emptySeries();
      });
  }

  emptySeries(): void {
    this.bicepsSeries = [];
    this.waistSeries = [];
    this.hipsSeries = [];
    this.weightSeries = [];
    this.chestSeries = [];
  }

  createForm(): void {
    this.filterForm = this.fb.group({
      start: '',
      end: '',
    });
  }

  formListener(): void {
    this.filterForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.filterValues = value;
      });
  }

  addToSeries(measurement: MeasurementInterface): void {
    const name = new Date(measurement.date);

    this.bicepsSeries.push({
      name,
      value: measurement.biceps,
    });

    this.waistSeries.push({
      name,
      value: measurement.waist,
    });

    this.hipsSeries.push({
      name,
      value: measurement.hips,
    });

    this.weightSeries.push({
      name,
      value: measurement.weight,
    });

    this.chestSeries.push({
      name,
      value: measurement.chest,
    });
  }

  applyFilters(reset?: boolean): void {
    if (reset) {
      this.filterForm.setValue({
        start: '',
        end: '',
      });
    }
    this.getMeasurements();
  }
}
