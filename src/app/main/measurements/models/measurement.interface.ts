import { UserInterface } from 'src/app/core/models/users/user.interface';

export interface MeasurementInterface {
  photoId: string;
  photoUrl: string;
  _id: string;
  weight: number;
  chest: number;
  waist: number;
  hips: number;
  biceps: number;
  date: Date;
  user: string;
}
