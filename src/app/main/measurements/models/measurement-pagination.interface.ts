import { MeasurementInterface } from './measurement.interface';

export interface MeasurementPaginationInterface {
  measurements: MeasurementInterface[];
  numOfMeasurements: number;
}
