import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, map } from 'rxjs';
import { MeasurementInterface } from '../models/measurement.interface';
import { UploadPhotoInterface } from '../models/upload-photo.interface';
import { MeasurementPaginationInterface } from '../models/measurement-pagination.interface';
import { environment } from '../../../../environments/environment';
import { removeFalsyFromFilter } from '../../../core/utils/remove-falsy-from-filter';
import { createParams } from '../../../core/utils/create-params';
import { GeneralFilterInterface } from '../../../core/models/filters/general-filter.interface';

@Injectable({
  providedIn: 'root',
})
export class MeasurementsService {
  constructor(private http: HttpClient) {}

  getMeasurements(
    id: string,
    skip: number,
    limit: number,
    filter?: GeneralFilterInterface
  ): Observable<MeasurementPaginationInterface> {
    const clearedFilter = removeFalsyFromFilter({
      ...filter,
      skip: skip ? skip.toString() : '',
      limit: limit ? limit.toString() : '',
    });
    const params = createParams(clearedFilter);

    return this.http
      .get<MeasurementPaginationInterface>(
        `${environment.apiUrl}/measurements/${id}`,
        {
          params: params,
        }
      )
      .pipe(
        map((res) => {
          return {
            ...res,
            measurements: res.measurements.map((m) => {
              return { ...m, date: new Date(m.date) };
            }),
          };
        })
      );
  }

  createMeasurement(
    measurement: MeasurementInterface
  ): Observable<MeasurementInterface> {
    return this.http.post<MeasurementInterface>(
      `${environment.apiUrl}/measurements`,
      measurement
    );
  }

  uploadPhoto(formData: FormData): Observable<UploadPhotoInterface> {
    return this.http.post<UploadPhotoInterface>(
      `${environment.apiUrl}/measurements/upload`,
      formData
    );
  }

  deletePhoto(name: string): Observable<{ result: string }> {
    return this.http.delete<{ result: string }>(
      `${environment.apiUrl}/measurements/delete/${name}`
    );
  }

  updateMeasurement(
    id: string,
    measurement: MeasurementInterface
  ): Observable<MeasurementInterface> {
    return this.http.patch<MeasurementInterface>(
      `${environment.apiUrl}/measurements/${id}`,
      measurement
    );
  }

  deleteMeasurement(id: string): Observable<MeasurementInterface> {
    return this.http.delete<MeasurementInterface>(
      `${environment.apiUrl}/measurements/${id}`
    );
  }
}
