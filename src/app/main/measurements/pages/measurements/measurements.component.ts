import { Component, OnInit, AfterViewInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { switchMap, takeUntil, filter, finalize } from 'rxjs';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MeasurementsService } from '../../services/measurements.service';
import { MeasurementPhotoComponent } from '../../components/measurement-photo/measurement-photo.component';
import { CreateEditMeasurementComponent } from '../../components/create-edit-measurement/create-edit-measurement.component';
import { MeasurementInterface } from '../../models/measurement.interface';
import { AbstractPaginator } from '../../../components/abstractions/abstract-paginator';
import { GeneralFilterInterface } from '../../../../core/models/filters/general-filter.interface';

@Component({
  selector: 'app-measurements',
  templateUrl: './measurements.component.html',
  styleUrls: [
    './measurements.component.scss',
    '../../../../styles/spinner.scss',
    '../../../../styles/actions-container.scss',
    '../../../../styles/table.scss',
  ],
})
export class MeasurementsComponent
  extends AbstractPaginator
  implements OnInit, AfterViewInit
{
  filterForm!: FormGroup;
  filterValues: GeneralFilterInterface = {
    start: '',
    end: '',
  };
  hasResults: boolean = true;
  measurements: MeasurementInterface[] = [];
  id!: string;

  isLoading: boolean = false;

  constructor(
    private dialog: MatDialog,
    private measurementsService: MeasurementsService,
    private route: ActivatedRoute,
    private snackBar: MatSnackBar,
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit(): void {
    this.createForm();
    this.formListener();
    this.getData(this.skip, this.limit);
  }

  ngAfterViewInit(): void {
    this.subscribeToPaginatorChanges();
  }

  createForm(): void {
    this.filterForm = this.fb.group({
      start: '',
      end: '',
    });
  }

  formListener(): void {
    this.filterForm.valueChanges
      .pipe(takeUntil(this.destroy$))
      .subscribe((value) => {
        this.filterValues = value;
      });
  }

  getData(skip?: number, limit?: number): void {
    this.isLoading = true;
    this.id = this.route.snapshot.paramMap.get('id')!;
    this.measurementsService
      .getMeasurements(
        this.id,
        skip ? skip : this.paginator.pageIndex + 1,
        limit ? limit : this.paginator.pageSize,
        this.filterValues
      )
      .pipe(
        finalize(() => (this.isLoading = false)),
        takeUntil(this.destroy$)
      )
      .subscribe(
        (res) => {
          this.measurements = res.measurements;
          this.hasResults = this.measurements.length ? true : false;
          this.count = res.numOfMeasurements;
        },
        (e) => {
          this.snackBar.open('Could not load measurements', 'Close');
        }
      );
  }

  openPhoto(url: string): void {
    this.dialog.open(MeasurementPhotoComponent, {
      data: { src: url },
    });
  }

  openCreateEdit(measurement: Partial<MeasurementInterface>): void {
    const dialogRef = this.dialog.open(CreateEditMeasurementComponent, {
      data: { ...measurement },
    });
    dialogRef
      .afterClosed()
      .pipe(
        filter((res) => res),
        switchMap((res) => {
          return this.measurementsService.getMeasurements(
            this.id,
            this.paginator.pageIndex + 1,
            this.paginator.pageSize,
            this.filterValues
          );
        }),
        takeUntil(this.destroy$)
      )
      .subscribe((res) => {
        this.measurements = res.measurements;
        this.hasResults = this.measurements.length ? true : false;
        this.count = res.numOfMeasurements;
      });
  }

  onDeleteMeasurement(measurement: MeasurementInterface): void {
    this.snackBar.open('Deleting...');
    const { _id, photoId } = measurement;
    this.measurementsService
      .deletePhoto(photoId)
      .pipe(
        switchMap(() =>
          this.measurementsService.deleteMeasurement(_id).pipe(
            switchMap(() => {
              return this.measurementsService.getMeasurements(
                this.id,
                this.paginator.pageIndex + 1,
                this.paginator.pageSize,
                this.filterValues
              );
            })
          )
        ),
        takeUntil(this.destroy$)
      )
      .subscribe((res) => {
        this.measurements = res.measurements;
        this.hasResults = this.measurements.length ? true : false;
        this.count = res.numOfMeasurements;
        this.snackBar.open('Measurement deleted', 'Close', {
          duration: 3000,
        });
      });
  }

  applyFilters(reset?: boolean): void {
    if (reset) {
      this.filterForm.setValue({
        start: '',
        end: '',
      });
    }
    this.getData(this.skip, this.limit);
  }
}
