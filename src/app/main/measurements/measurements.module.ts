import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatSortModule } from '@angular/material/sort';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatPaginatorModule } from '@angular/material/paginator';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { MeasurementsRoutingModule } from './measurements-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { MeasurementsComponent } from './pages/measurements/measurements.component';
import { MeasurementPhotoComponent } from './components/measurement-photo/measurement-photo.component';
import { CreateEditMeasurementComponent } from './components/create-edit-measurement/create-edit-measurement.component';
import { MeasurementsGraphComponent } from './components/measurements-graph/measurements-graph.component';

@NgModule({
  declarations: [
    MeasurementsComponent,
    MeasurementPhotoComponent,
    CreateEditMeasurementComponent,
    MeasurementsGraphComponent,
  ],
  entryComponents: [MeasurementPhotoComponent, CreateEditMeasurementComponent],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    MeasurementsRoutingModule,
    MatTabsModule,
    MatTableModule,
    MatDialogModule,
    MatIconModule,
    MatButtonModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSnackBarModule,
    SharedModule,
    MatSortModule,
    MatPaginatorModule,
    MatProgressSpinnerModule,
    NgxChartsModule,
  ],
})
export class MeasurementsModule {}
