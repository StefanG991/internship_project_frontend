import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LocalStorageService } from '../../../../core/services/local-storage.service';
import { AuthenticationService } from '../../../../auth/services/authentication.service';

@Component({
  selector: 'app-main-header',
  templateUrl: './main-header.component.html',
  styleUrls: ['./main-header.component.scss', '../../../../styles/header.scss'],
})
export class MainHeaderComponent implements OnInit {
  id: string | undefined;

  constructor(
    private authService: AuthenticationService,
    private localStorageService: LocalStorageService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.id = this.authService.getUserInfo().user._id;
  }

  onLogout(): void {
    this.localStorageService.clearStorage();
    this.router.navigate(['/auth/login']);
  }
}
