import { Component, OnDestroy, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Subject, takeUntil, tap } from 'rxjs';

@Component({ template: '' })
export abstract class AbstractPaginator implements OnDestroy {
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  destroy$: Subject<void> = new Subject();
  limit: number = 6;
  skip: number = 1;
  count!: number;

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  subscribeToPaginatorChanges(): void {
    this.paginator.page
      .pipe(
        tap(() => this.getData()),
        takeUntil(this.destroy$)
      )
      .subscribe();
  }

  abstract getData(): void;
}
