import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ErrorComponent } from './components/error/error.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';

@NgModule({
  declarations: [ErrorComponent, PageNotFoundComponent],
  imports: [CommonModule, MatFormFieldModule],
  exports: [ErrorComponent],
})
export class SharedModule {}
