import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Subject, takeUntil } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { PasswordValidator } from 'src/app/core/utils/password.validator';
import { AuthenticationService } from '../../services/authentication.service';
import { Gender } from '../../../core/enums/gender.enum';
import { passwordRegex } from 'src/app/core/constants/regex';
import { EmailValidator } from 'src/app/core/utils/email.validator';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss', '../../../styles/forms.scss'],
})
export class RegistrationComponent implements OnInit, OnDestroy {
  registerForm!: FormGroup;
  destroy$: Subject<void> = new Subject();
  genderArray: string[] = Object.values(Gender);

  constructor(
    private authService: AuthenticationService,
    private fb: FormBuilder,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.registerForm = this.fb.group({
      name: [null, [Validators.required]],
      email: [null, [Validators.required, EmailValidator]],
      password: [
        null,
        [Validators.required, Validators.pattern(passwordRegex)],
      ],
      confirmPassword: [null, [Validators.required, PasswordValidator]],
      gender: [null, [Validators.required]],
      dateOfBirth: [null, [Validators.required]],
      height: [null, [Validators.required]],
    });
    this.triggerConfirmPasswordValidation();
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }

  triggerConfirmPasswordValidation(): void {
    this.registerForm
      .get('password')
      ?.valueChanges.pipe(takeUntil(this.destroy$))
      .subscribe(() => {
        this.registerForm.get('confirmPassword')?.updateValueAndValidity();
      });
  }

  onSubmit(): void {
    if (this.registerForm.invalid) {
      return;
    }
    this.authService
      .register(this.registerForm.value)
      .pipe(takeUntil(this.destroy$))
      .subscribe({
        next: () => {
          this.router.navigate(['/auth/login']);
          this.registerForm.reset();
        },
        error: (error) => {
          this.snackBar.open(`${error.error.message}`, 'Close', {
            duration: 3000,
          });
        },
      });
  }

  getControl(control: string): AbstractControl | null {
    return this.registerForm.get(control);
  }
}
