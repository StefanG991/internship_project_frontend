import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, tap } from 'rxjs';
import { LocalStorageService } from '../../core/services/local-storage.service';
import { UserInterface } from '../../core/models/users/user.interface';
import { ExistingUserInterface } from '../models/existing-user.interface';
import { TokenInterface } from '../models/token.interface';
import { environment } from '../../../environments/environment';
import { getDecodedAccessToken } from '../../core/utils/get-decoded-access-token';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(
    private http: HttpClient,
    private localStorageService: LocalStorageService
  ) {}

  register(user: UserInterface): Observable<UserInterface> {
    return this.http.post<UserInterface>(
      `${environment.apiUrl}/auth/register`,
      user
    );
  }

  login(user: ExistingUserInterface): Observable<TokenInterface> {
    return this.http
      .post<TokenInterface>(`${environment.apiUrl}/auth/login`, user)
      .pipe(
        tap((res) => {
          const userInfo = getDecodedAccessToken(res.token)!;
          this.localStorageService.setItem(
            'userInfo',
            JSON.stringify(userInfo)
          );
          this.localStorageService.setItem('token', res.token);
        })
      );
  }

  getToken(): string | null {
    return this.localStorageService.getItem('token');
  }

  getUserInfo(): { user: UserInterface } {
    return JSON.parse(this.localStorageService.getItem('userInfo')!);
  }
}
